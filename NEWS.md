


sfSpHelpers v0.4 326e4426b252b867cf4133a061af4ce01c914d2c Apr 24
==============

Changes:

* Implemented osm bbox + bbox from vector
* Removed useless makeValid functions
* Debugged vignettes + added voronoi vignette


sfSpHelpers v0.3 c5e9ee7c2c86c9cd2672e463fa581371e22ae34c Apr 23
==============

Changes:

* Implemented linestring split functions
* Removed useless dependencies - e.g. GeneralHelpers
* Removed useless functions that could be replaced by existing e.g st_read_remote is the same as st_read
* Debugged functions: get_zipped_remote_shapefile 


sfSpHelpers v0.2 - ce153047ea9b36273f95d89987d3b8b82f239410  Mon Mar 28
==============

Changes:

* Implemented osmIsochrone functions



sfSpHelpers v0.1 Sun Jan 17  b297a4c2e839f62a724e44747d0b1d721f457140
==============

Changes:

* New polygon scaling function


sfSpHelpers v0.0 Tue Aug 25 2020  372f03bfab48f818db4aeb8d2be61ee05a8dad6c
==============

Changes:

* New k-means function + voronoi interpolation


